<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The python-rdflib-tools package (tools for converting to and from RDF)
had wrappers that could load Python modules from the current
working directory, allowing code injection.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
4.2.1-2+deb9u1.</p>

<p>We recommend that you upgrade your rdflib packages.</p>

<p>For the detailed security status of rdflib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rdflib">https://security-tracker.debian.org/tracker/rdflib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2861.data"
# $Id: $
