<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An authenticated remote attacker can execute arbitrary code in
Firebird, a relational database based on InterBase 6.0, by executing a
malformed SQL statement. The only known solution is to disable
external UDF libraries from being loaded. In order to achieve this,
the default configuration has changed to UdfAccess=None. This will
prevent the fbudf module from being loaded, but may also break other
functionality relying on modules.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.0.1.32609.ds4-14+deb9u1.</p>

<p>We recommend that you upgrade your firebird3.0 packages.</p>

<p>For the detailed security status of firebird3.0 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firebird3.0">https://security-tracker.debian.org/tracker/firebird3.0</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2824.data"
# $Id: $
