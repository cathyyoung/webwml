<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential memory corruption vulnerability
in the lz4 compression algorithm library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3520">CVE-2021-3520</a>

    <p>memory corruption due to an integer overflow bug caused by memmove
    argument</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
0.0~r131-2+deb9u1.</p>

<p>We recommend that you upgrade your lz4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2657.data"
# $Id: $
