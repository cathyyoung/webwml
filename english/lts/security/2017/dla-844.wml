<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Marco <q>nemux</q> Romano discovered that an integer overflow in the
quicktime_read_pascal function in libquicktime 1.2.4 and earlier
allows remote attackers to cause a denial of service or possibly have
other unspecified impact via a crafted hdlr MP4 atom.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:1.2.4-3+deb7u1.</p>

<p>We recommend that you upgrade your libquicktime packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-844.data"
# $Id: $
