<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that PHP5 was vulnerable to a reflected cross-site
scripting (XSS) attack on the PHAR 404 error page by manipulating the
URI of a request for a .phar file. This issue is only exploitable if
the web server is configured to handle phar files using PHP5.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.4.45-0+deb7u12.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1251.data"
# $Id: $
