<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An authenticated remote attacker can execute arbitrary code in Firebird SQL
Server versions 2.5.7 and 3.0.2 by executing a malformed SQL statement. The
only known solution is to disable external UDF libraries from being loaded.  In
order to achieve this, the default configuration has changed to UdfAccess=None.
This will prevent the fbudf module from being loaded, but may also break other
functionality relying on modules.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.5.2.26540.ds4-1~deb7u4.</p>

<p>We recommend that you upgrade your firebird2.5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1374.data"
# $Id: $
