<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were found in libarchive, a
multi-format archive and compression library. Heap-based buffer
over-reads, NULL pointer dereferences and out-of-bounds reads allow
remote attackers to cause a denial-of-service (application crash) via
specially crafted archive files.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.1.2-11+deb8u4.</p>

<p>We recommend that you upgrade your libarchive packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1600.data"
# $Id: $
