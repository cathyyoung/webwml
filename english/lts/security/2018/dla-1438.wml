<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Early versions of opencv have problems while reading data, which might
result in either buffer overflows, out-of bounds errors or integer
overflows.
Further assertion errors might happen due to incorrect integer cast.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.4.9.1+dfsg-1+deb8u2.</p>

<p>We recommend that you upgrade your opencv packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1438.data"
# $Id: $
