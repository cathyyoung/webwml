<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A use-after-free in onig_new_deluxe() in regext.c allows attackers to
potentially cause information disclosure, denial of service, or
possibly code execution by providing a crafted regular expression. The
attacker
provides a pair of a regex pattern and a string, with a multi-byte
encoding that gets handled by onig_new_deluxe().</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
5.9.5-3.2+deb8u2.</p>

<p>We recommend that you upgrade your libonig packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1854.data"
# $Id: $
