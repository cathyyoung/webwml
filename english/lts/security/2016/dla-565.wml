<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in the implementation of the
Perl programming language. The Common Vulnerabilities and Exposures
project identifies the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1238">CVE-2016-1238</a>

    <p>John Lightsey and Todd Rinaldo reported that the opportunistic
    loading of optional modules can make many programs unintentionally
    load code from the current working directory (which might be changed
    to another directory without the user realising) and potentially
    leading to privilege escalation, as demonstrated in Debian with
    certain combinations of installed packages.</p>

    <p>The problem relates to Perl loading modules from the includes
    directory array ("@INC") in which the last element is the current
    directory ("."). That means that, when <q>perl</q> wants to load a module
    (during first compilation or during lazy loading of a module in run
    time), perl will look for the module in the current directory at the
    end, since '.' is the last include directory in its array of include
    directories to seek. The issue is with requiring libraries that are
    in "." but are not otherwise installed.</p>

    <p>With this update several modules which are known to be vulnerable
    are updated to not load modules from current directory.</p>

    <p>Additionally the update allows configurable removal of "." from @INC
    in /etc/perl/sitecustomize.pl for a transitional period. It is
    recommended to enable this setting if the possible breakage for a
    specific site has been evaluated. Problems in packages provided in
    Debian resulting from the switch to the removal of '.' from @INC
    should be reported to the Perl maintainers at
    perl@packages.debian.org .</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6185">CVE-2016-6185</a>

    <p>It was discovered that XSLoader, a core module from Perl to
    dynamically load C libraries into Perl code, could load shared
    library from incorrect location. XSLoader uses caller() information
    to locate the .so file to load. This can be incorrect if
    XSLoader::load() is called in a string eval. An attacker can take
    advantage of this flaw to execute arbitrary code.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.14.2-21+deb7u4.</p>

<p>We recommend that you upgrade your perl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-565.data"
# $Id: $
