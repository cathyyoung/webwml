<define-tag description>security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4122">CVE-2021-4122</a>

    <p>Milan Broz, its maintainer, discovered an issue in cryptsetup, the disk
    encryption configuration tool for Linux.</p>

    <p>LUKS2 (an on-disk format) online reencryption is an optional extension to
    allow a user to change the data reencryption key while the data device is
    available for use during the whole reencryption process.</p>

    <p>An attacker can modify on-disk metadata to simulate decryption in progress
    with crashed (unfinished) reencryption step and persistently decrypt part
    of the LUKS2 device (LUKS1 devices are indirectly affected as well, see
    below).</p>

    <p>This attack requires repeated physical access to the LUKS2 device but no
    knowledge of user passphrases.</p>

    <p>The decryption step is performed after a valid user activates the device
    with a correct passphrase and modified metadata.</p>

    <p>The size of possible decrypted data per attack step depends on configured
    LUKS2 header size (metadata size is configurable for LUKS2). With the
    default LUKS2 parameters (16 MiB header) and only one allocated keyslot
    (512 bit key for AES-XTS), simulated decryption with checksum resilience
    SHA1 (20 bytes checksum for 4096-byte blocks), the maximal decrypted size
    can be over 3GiB.</p>

    <p>The attack is not applicable to LUKS1 format, but the attacker can update
    metadata in place to LUKS2 format as an additional step. For such a
    converted LUKS2 header, the keyslot area is limited to decrypted size (with
    SHA1 checksums) over 300 MiB.</p>

    <p>LUKS devices that were formatted using a cryptsetup binary from Debian
    Stretch or earlier are using LUKS1. However since Debian Buster the default
    on-disk LUKS format version is LUKS2. In particular, encrypted devices
    formatted by the Debian Buster and Bullseye installers are using LUKS2 by
    default.</p></li>

<li>Key truncation in dm-integrity

    <p>This update additionaly fixes a key truncation issue for standalone
    dm-integrity devices using HMAC integrity protection. For existing such
    devices with extra long HMAC keys (typically &gt;106 bytes of length), one
    might need to manually truncate the key using integritysetup(8)'s
    <q>--integrity-key-size</q> option in order to properly map the device under
    2:2.3.7-1+deb11u1 and later.</p>

    <p>Only standalone dm-integrity devices are affected. dm-crypt devices,
    including those using authenticated disk encryption, are unaffected.</p></li>

</ul>

<p>For the oldstable distribution (buster), this problem is not present.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2:2.3.7-1+deb11u1.</p>

<p>We recommend that you upgrade your cryptsetup packages.</p>

<p>For the detailed security status of cryptsetup please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cryptsetup">\
https://security-tracker.debian.org/tracker/cryptsetup</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5070.data"
# $Id: $
