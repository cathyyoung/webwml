# EG <galatoulas@cti.gr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2021-09-02 01:59+0300\n"
"Last-Translator: EG <galatoulas@cti.gr>\n"
"Language-Team: Greek <debian-l10n-greek@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.0\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Το Οικουμενικό Λειτουργικό Σύστημα"

#: ../../english/index.def:12
msgid "DC19 Group Photo"
msgstr "Ομαδική φωτογραφία από το DC19"

#: ../../english/index.def:15
msgid "DebConf19 Group Photo"
msgstr "Ομαδική φωτογραφία από το DebConf19"

#: ../../english/index.def:19
msgid "Mini DebConf Hamburg 2018"
msgstr "Mini DebConf Αμβούργο 2018"

#: ../../english/index.def:22
msgid "Group photo of the MiniDebConf in Hamburg 2018"
msgstr "Ομαδική φωτογραφία από το MiniDebConf στο Αμβούργο το 2018"

#: ../../english/index.def:26
msgid "Screenshot Calamares Installer"
msgstr "Στιγμιότυπο οθόνης του εγκαταστάτη Calamares"

#: ../../english/index.def:29
msgid "Screenshot from the Calamares installer"
msgstr "Στιγμιότυπο οθόνης του εγκαταστάτη Calamares"

#: ../../english/index.def:33 ../../english/index.def:36
msgid "Debian is like a Swiss Army Knife"
msgstr "Το Debian είναι σαν ένας ελβετικός σουγιάς"

#: ../../english/index.def:40
msgid "People have fun with Debian"
msgstr "Ο κόσμος διασκεδάζει με το Debian"

#: ../../english/index.def:43
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "Μέλη του Debian στο Debconf18 στο Hsinchu διασκεδάζοντας πραγματικά"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "Διάφορα από το Debian"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Ιστολόγιο"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Μικρά Νέα"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Μικρά Νέα από το Debian"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Πλανήτης"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "Ο Πλανήτης του Debian"
