#use wml::debian::translation-check translation="b917e690cbacd447496fcc36bb3b22df5d6873b2" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В HTTPD-сервере Apache было обнаружено несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9517">CVE-2019-9517</a>

    <p>Джонатан Луни сообщил, что злоумышленник может вызвать отказ
    в обслуживании (исчерпание ресурсов исполняемых модулей h2) путём
    заполнения соединения запросами без чтения ответов по
    TCP-соединению.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10081">CVE-2019-10081</a>

    <p>Крейг Янг сообщил, что HTTP/2 PUSH может приводить к перезаписи
    содержимого памяти в пуле отправляемого запроса, что вызывает аварийную остановку.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10082">CVE-2019-10082</a>

    <p>Крейг Янг сообщил, что обработка сессии HTTP/2 может вызывать чтение
    памяти после её освобождения в ходе завершения соединения.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10092">CVE-2019-10092</a>

    <p>Матеи <q>Mal</q> Бадану сообщил об ограниченном межсайтовом скриптинге
    на странице ошибки mod_proxy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10097">CVE-2019-10097</a>

    <p>Дэниель Маккарни сообщил, что когда mod_remoteip настроен на использование
    доверенного промежуточного прокси, использующего протокол <q>PROXY</q>,
    то специально сформированный заголовок PROXY может вызвать переполнение буфера
    или разыменование NULL-указателя. Эта уязвимость может использоваться только
    доверенным прокси, но не может использоваться недоверенными HTTP-клиентами. Проблема
    не касается выпуска stretch.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10098">CVE-2019-10098</a>

    <p>Юкитсугу Сасаки сообщил о потенциальном открытом перенаправлении в
    модуле mod_rewrite.</p></li>

</ul>

<p>В предыдущем стабильном выпуске (stretch) эти проблемы были исправлены
в версии 2.4.25-3+deb9u8.</p>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 2.4.38-3+deb10u1.</p>

<p>Рекомендуется обновить пакеты apache2.</p>

<p>С подробным статусом поддержки безопасности apache2 можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4509.data"
