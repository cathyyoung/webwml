#use wml::debian::translation-check translation="7ce619292bb7825470d6cb611887d68f405623ff"
<define-tag pagetitle>Debian 11 <q>bullseye</q> publicado</define-tag>
<define-tag release_date>2021-08-14</define-tag>
#use wml::debian::news


<p>Después de dos años, un mes y nueve días de desarrollo, el proyecto
Debian se complace en presentar la nueva versión «estable» 11 (nombre en clave <q>bullseye</q>),
a la que se dará soporte durante los próximos cinco años gracias al trabajo combinado de los
equipos de <a href="https://security-team.debian.org/">seguridad</a> 
y de <a href="https://wiki.debian.org/LTS">soporte a largo plazo</a> de Debian.
</p>

<p>
Debian 11 <q>bullseye</q> se publica con varios entornos y aplicaciones para el
escritorio. Ahora incluye, entre otros, los entornos de escritorio:
</p>
<ul>
<li>Gnome 3.38,</li>
<li>KDE Plasma 5.20,</li>
<li>LXDE 11,</li>
<li>LXQt 0.16,</li>
<li>MATE 1.24,</li>
<li>Xfce 4.16.</li>
</ul>


<p>Esta versión contiene un total de 59&nbsp;551 paquetes de los que más de 11&nbsp;294 son
nuevos, y esto a pesar de una reducción significativa de más de 9519 paquetes que se marcaron como
<q>obsoletos</q> y fueron eliminados. 42&nbsp;821 paquetes se han actualizado y 5434 permanecen sin
modificar.
</p>

<p>
<q>bullseye</q> es nuestra primera versión en proporcionar un núcleo Linux con soporte para el
sistema de ficheros exFAT y, por omisión, lo utiliza para montar sistemas de ficheros exFAT.
En consecuencia, ya no es necesario utilizar la implementación de
filesystem-in-userspace proporcionada por el paquete exfat-fuse. El paquete exfatprogs
proporciona herramientas para definir y comprobar sistemas de ficheros exFAT.
</p>


<p>
La mayoría de las impresoras modernas son capaces de utilizar impresión y escaneo sin controlador, haciendo
innecesario el uso de controladores específicos del fabricante (a menudo no libres).

<q>bullseye</q> introduce un nuevo paquete, ipp-usb, que utiliza el protocolo IPP-over-USB,
independiente del fabricante y soportado por muchas impresoras actuales. Esto permite que un dispositivo
USB sea tratado como un dispositivo de red. El backend oficial SANE sin controlador
es proporcionado por sane-escl en libsane1, que utiliza el protocolo eSCL.
</p>

<p>
Systemd en <q>bullseye</q> activa su funcionalidad de journal persistente, por omisión,
con vuelta implícita a almacenamiento volátil en caso necesario. Esto permite a los usuarios y usuarias que no
precisan funcionalidades especiales desinstalar los demonios tradicionales de escritura de logs y
pasar a usar exclusivamente el journal de systemd.
</p>

<p>
El equipo de Debian Med ha estado tomando parte en la lucha contra la COVID-19 
empaquetando software para la investigación del virus a nivel de secuencia
y para la lucha contra la pandemia con las herramientas utilizadas en epidemiología; esta labor
continuará enfocada en herramientas de aprendizaje automatizado para ambos campos. El trabajo
del equipo en control de calidad y en integración continua es crítico para obtener
resultados reproducibles y consistentes, como se requiere en el ámbito científico.

Debian Med Blend tiene una serie de aplicaciones en las que el rendimiento es crítico y que ahora
se benefician de SIMD Everywhere. Para instalar los paquetes administrados por el equipo de Debian
Med, instale los metapaquetes llamados med-*, que están en la versión 3.6.x. 
</p>

<p>
Los idiomas chino, japonés, coreano y muchos otros tienen ahora un nuevo método de entrada:
Fcitx 5, sucesor del popular Fcitx4 de <q>buster</q>; esta nueva versión
tiene un soporte mucho mejor de extensiones de Wayland (el gestor de pantalla por omisión).
</p>

<p>
Más del 72% de los paquetes han sido actualizados en Debian 11 <q>bullseye</q>, que 
incluye nuevas versiones de programas como:
</p>
<ul>
<li>Apache 2.4.48</li>
<li>BIND DNS Server 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>GNU Compiler Collection 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>Linux kernel 5.10 series</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP 7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>y más de 59&nbsp;000 paquetes listos para utilizar, construidos a partir de
más de 30&nbsp;000 paquetes fuente.</li>
</ul>

<p>
Con esta extensa selección de software y su tradicional amplio
soporte de arquitecturas, Debian se mantiene fiel a su objetivo de ser
el sistema operativo universal. Es una elección apropiada para muchos casos
de uso: desde sistemas de escritorio hasta netbooks; desde entornos de desarrollo hasta
sistemas en cluster; y para servidores web, de almacenamiento y de bases de datos. De igual
forma, el trabajo de control de calidad que incluye instalación automática
y pruebas de actualización para todos los paquetes del archivo de Debian ayuda a que
<q>bullseye</q> satisfaga las altas expectativas que los usuarios y usuarias tienen de una
versión «estable» de Debian.
</p>

<p>
En total, se proporciona soporte a nueve arquitecturas:
PC de 64 bits / Intel EM64T / x86-64 (<code>amd64</code>),
PC de 32 bits / Intel IA-32 (<code>i386</code>),
Motorola/IBM PowerPC de 64 bits little-endian  (<code>ppc64el</code>),
IBM S/390 de 64 bits (<code>s390x</code>),
para ARM, <code>armel</code>
y <code>armhf</code> para hardware de 32 bits tanto antiguo como más reciente,
además de <code>arm64</code> para la arquitectura de 64 bits <q>AArch64</q>,
y para MIPS, <code>mipsel</code> (little-endian) en cuanto a 32 bits 
y <code>mips64el</code> para el hardware little-endian de 64 bits.
</p>

<h3>¿Quiere probarlo?</h3>
<p>
Si simplemente quiere probar Debian 11 <q>bullseye</q> sin instalarlo,
puede utilizar una de las <a href="$(HOME)/CD/live/">imágenes en vivo («live»)</a> que cargan y ejecutan
el sistema operativo en modo de solo lectura en la memoria del ordenador.
</p>

<p>
Estas imágenes se ofrecen para las arquitecturas <code>amd64</code> e
<code>i386</code> para su instalación en DVD, memorias USB
e instalaciones netboot. Los usuarios pueden elegir qué entornos
de escritorio probar: GNOME, KDE Plasma, LXDE, LXQt, MATE y Xfce.
Debian Live <q>bullseye</q> contiene también una imagen «live» estándar, que permite
probar un sistema Debian sin interfaz gráfica.
</p>

<p>
Si le satisface el sistema operativo, tiene la opción de instalarlo
en el disco duro de su ordenador desde la imagen en vivo. Esta incluye
el instalador independiente Calamares además del instalador estándar de Debian.
Hay más información disponible en las secciones
<a href="$(HOME)/releases/bullseye/releasenotes">notas de publicación</a> e
<a href="$(HOME)/CD/live/">imágenes de instalación en vivo</a>
del sitio web de Debian.
</p>

<p>
Para instalar Debian 11 <q>bullseye</q> directamente en el
disco duro puede elegir otros medios de instalación
tales como discos Blu-ray, DVD, CD, memorias USB o a través de la red.
Puede instalar varios entornos de escritorio como Cinnamon, GNOME, escritorio y
aplicaciones KDE Plasma, LXDE, LXQt, MATE o Xfce con estas
imágenes.
Además, los CD de <q>múltiples arquitecturas</q> permiten
instalar varias arquictecturas desde un mismo medio de instalación. También puede
crear medios de instalación USB autoarrancables
(vea la <a href="$(HOME)/releases/bullseye/installmanual">Guía de instalación</a>
para más detalles).
</p>

# Translators: some text taken from /devel/debian-installer/News/2021/20210802

<p>
Ha habido mucho trabajo de desarrollo en el instalador de Debian,
que ahora incluye un soporte mejorado de hardware y otras funcionalidades nuevas.
</p>
<p>
En algunos casos, a pesar de que la instalación se complete sin errores pueden presentarse problemas con la pantalla
al arrancar el sistema instalado; para estas situaciones hay
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">algunas soluciones alternativas</a>
que pueden ayudar a acceder al sistema.
También hay un
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">procedimiento utilizando isenkram</a>
que permite a los usuarios detectar y corregir, de manera automatizada, la ausencia de
firmware en el sistema. Por supuesto, los usuarios y usuarias tienen que sopesar los pros y
los contras de utilizar esta herramienta ya que es muy probable que necesite
instalar paquetes no libres.</p>

<p>
  Aparte de esto, se han mejorado las
  <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">imágenes no libres del instalador que incluyen paquetes con firmware</a>
  de manera que puedan anticipar la necesidad de firmware
  en el sistema instalado (por ejemplo, firmware para tarjetas gráficas AMD o
  Nvidia o para generaciones recientes de hardware de audio Intel).
</p>

<p>
Para usuarios de la nube, Debian ofrece soporte directo para muchas
plataformas de nube. Hay imágenes oficiales de Debian que se pueden
escoger en las galerías de los proveedores. Debian también publica <a
href="https://cloud.debian.org/images/openstack/current/">imágenes
pregeneradas para OpenStack</a> para <code>amd64</code> y <code>arm64</code>,
que puede descargar y usar en una nube local.
</p>

<p>
Debian se puede instalar en 76 idiomas distintos, la mayoría de ellos disponibles
tanto en interfaces de texto como gráficas.
</p>

<p>
Las imágenes de instalación se pueden descargar a través de:
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (recomendado),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> o
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; vea
<a href="$(HOME)/CD/">Debian en CD</a> para más información. <q>bullseye</q>
estará también disponible físicamente en DVD, CD-ROM y discos Blu-ray
a través de numerosos <a href="$(HOME)/CD/vendors">vendedores</a>.
</p>


<h3>Actualización de Debian</h3>
<p>
Para la mayoría de las configuraciones, la herramienta de gestión de paquetes
APT maneja automáticamente las actualizaciones a Debian 11
desde la versión anterior, Debian 10 (<q>buster</q>).
</p>

<p>
La distribución de actualizaciones de seguridad para bullseye se llama bullseye-security 
y los usuarios y usuarias deberían adaptar los ficheros source-list de APT al actualizar a Debian 11.
Si la configuración de APT incluye «pinning» o <code>APT::Default-Release</code>, 
es probable que también necesite ajustes. Consulte la sección
<a href="https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information#security-archive">Cambio de la organización del archivo de seguridad</a>
de las notas de publicación para más detalles.
</p>

<p>
Si va a actualizar en remoto, tenga en cuenta la sección
<a href="$(HOME)/releases/bullseye/amd64/release-notes/ch-information#ssh-not-available">Es posible que no se puedan definir conexiones SSH nuevas durante la actualización</a>. 
</p>

<p>
Como siempre, los sistemas Debian pueden actualizarse in situ, sin problemas
o tiempo de inactividad, pero se recomienda encarecidamente leer
las <a href="$(HOME)/releases/bullseye/releasenotes">Notas de publicación</a>
y la <a href="$(HOME)/releases/bullseye/installmanual">Guía de
instalación</a> para informarse de posibles problemas y obtener instrucciones detalladas para
instalar y actualizar Debian. En las próximas semanas, las Notas de instalación
continuarán mejorándose y traduciéndose a más idiomas.
</p>


<h2>Acerca de Debian</h2>

<p>
Debian es un sistema operativo libre desarrollado por
miles de voluntarios de todo el mundo que colaboran a través de
Internet. Los puntos fuertes del proyecto Debian son su base de voluntarios, su
dedicación al contrato social de Debian y al software libre, y su
compromiso por ofrecer el mejor sistema operativo posible. Esta nueva
versión es otro paso importante en esa dirección.
</p>


<h2>Información de contacto</h2>

<p>
Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a> o envíe un correo electrónico a
&lt;press@debian.org&gt;.
</p>
