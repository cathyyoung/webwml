#use wml::debian::projectnews::header PUBDATE="2020-09-09" SUMMARY="Le damos la bienvenida a las DPN, Canales de comunicación de Debian, Nuevo DPL, Noticias internas y externas, DebConf20 y eventos, Informes y Peticiones de ayuda"
#use wml::debian::acronyms
#use wml::debian::translation-check translation="de00676864ba33b45c4e42789e4ea68c0bacd001"

# Status: [Frozen]


## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<intro issue="primera" />

<toc-add-entry name="newdpn">¡Le damos la bienvenida a las noticias del proyecto Debian!</toc-add-entry>

<p>Esperamos que disfrute con esta <b>edición especial de archivo</b> de las noticias
del proyecto Debian que repite, presenta e intenta ponerle al día con la mayoría de
las noticias de lo que llevamos de 2020.</p>

<p>
En esta edición especial de las noticias hemos omitido algunas
secciones habituales en un esfuerzo por actualizar a la fecha nuestra cobertura. Si desea
encontrar información anterior o no incluida aquí, consulte nuestro
blog <a href="https://bits.debian.org">Bits de Debian</a> o nuestro
servicio <a href="https://micronews.debian.org">Debian Micronews</a>, donde todas
las noticias actuales y recientes han sido ya publicadas a lo largo del año.
</p>

<toc-add-entry name="official">Canales oficiales de comunicación de Debian.</toc-add-entry>

<p>
De vez en cuando recibimos consultas sobre eventos actuales, las normas acerca de quién puede
ser propietario de sitios web en el espacio de nombres de Debian, el estado de progreso del desarrollo en la
comunidad o los canales oficiales de comunicación
con y desde Debian.
</p>

<p>
La sección <a href="https://www.debian.org/News/">Últimas noticias</a> del sitio web de Debian 
contiene anuncios e información formales, muchos de los cuales son réplicas
extraídas de las listas de correo debian-news y debian-announce.
</p>

<p>
Nuestro blog <a href="https://bits.debian.org/">Bits de Debian</a> contiene noticias
extraídas de varias listas de correo de Debian junto con anuncios e información
semiformales siguiendo un ciclo de publicación mucho más breve.
</p>

<p>
Nuestro servicio <a href="https://micronews.debian.org/">Micronews</a> contiene y
cubre noticias breves, de impacto inmediato, y eventos de última hora. El
servicio Micronews también alimenta varios medios sociales como twitter,
identi.ca y framapiaf.org.
</p>

<p>
Por supuesto, no podemos olvidar el medio que está leyendo en estos momentos: las
<a href="https://www.debian.org/News/weekly/">noticias del proyecto Debian</a> son
el boletín del proyecto, que se publica de forma algo aleatoria (se necesita ayuda, ver más abajo).
</p>

<p>
Tenga en cuenta que todos los canales de comunicación enumerados está disponibles a través
del protocolo <a href="https://en.wikipedia.org/wiki/HTTPS"><b>https://</b></a>, 
que es usado en la totalidad de nuestro sitio web y de los canales oficiales.
</p>

<p>
Si necesita plantearnos cuestiones relacionadas con un anuncio o noticia,
no dude en ponerse en contacto con nosotros enviando un correo electrónico a
<a href="mailto:press@debian.org">press@debian.org</a>. Le rogamos que
tenga en cuenta que los equipos de prensa y de publicidad que <i>proporcionan estos
servicios con fines exclusivamente informativos</i> no tienen capacidad para dar soporte directo a los usuarios; las solicitudes de soporte
deberían dirigirse a la
<a href="https://lists.debian.org/debian-user/">lista de correo debian-user</a> o
al <a href="https://wiki.debian.org/Teams">equipo Debian</a> adecuado.
</p>

<toc-add-entry name="helpspread">¡Petición de voluntarios y de ayuda por parte del equipo de publicidad!</toc-add-entry>

<p>El equipo de publicidad pide ayuda a sus lectores, a desarrolladores y a
todas las partes interesadas para que contribuyan al esfuerzo informativo de Debian. Les rogamos que
envíen temas que puedan ser de interés para nuestra comunidad y también pedimos su
colaboración con la traducción de las noticias a (¡sus!) otros idiomas y con la aportación
del segundo o tercer ojo necesario para ayudar en la edición de nuestro trabajo antes de su
publicación. Si pueden aportar una pequeña cantidad de su tiempo para ayudar al equipo que
trata de mantenernos a todos informados, les necesitamos. Pónganse en contacto con nosotros por medio de IRC
en <a href="irc://irc.debian.org/debian-publicity">&#35;debian-publicity</a> en
<a href="https://oftc.net/">OFTC.net</a> o en nuestra
<a href="mailto:debian-publicity@lists.debian.org">lista pública de correo</a>,
o enviando un correo electrónico a <a href="mailto:press@debian.org">press@debian.org</a> para
consultas sensibles o privadas.</p>

<toc-add-entry name="longlivetheking">Hemos elegido un nuevo DPL.</toc-add-entry>

<p>Jonathan Carter (highvoltage), el recientemente elegido DPL, comparte sus pensamientos,
da las gracias y esboza las líneas generales del futuro de Debian en su primera entrada oficial de
<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00013.html">blog</a>
como DPL. Las publicaciones de blog del nuevo DPL están <a href="https://bits.debian.org/dpl/">publicadas</a> también
en el blog oficial de Debian.
</p>

<toc-display/>

<toc-add-entry name="security">Avisos de seguridad</toc-add-entry>

<p>El equipo de seguridad de Debian publica diariamente avisos
(<a href="$(HOME)/security/2020/">Avisos de seguridad de 2020</a>).
Léalos con atención y suscríbase a la <a href="https://lists.debian.org/debian-security-announce/">lista de correo de
seguridad</a>.</p>

<p>El sitio web de Debian ahora <a href="https://www.debian.org/lts/security/">archiva</a> también
los avisos de seguridad publicados por el equipo de soporte a largo plazo de Debian (LTS, por sus siglas en inglés) y enviados a la
<a href="https://lists.debian.org/debian-lts-announce/">lista de correo debian-lts-announce</a>.
</p>

<toc-add-entry name="internal">Noticias internas</toc-add-entry>

<p><b>Noticias de Debian 10 <q>buster</q>: publicada la versión 10.5</b></p>

<p>En julio de 2019 dimos la bienvenida a
<a href="https://www.debian.org/News/2019/20190706">la publicación de Debian 10
(nombre en clave <q>buster</q>)</a>. Desde entonces, 
el proyecto Debian ha anunciado la
<a href="https://www.debian.org/News/2019/20190907">primera</a>,
<a href="https://www.debian.org/News/2019/20191116">segunda</a>,
<a href="https://www.debian.org/News/2020/20200208">tercera</a>,
<a href="https://www.debian.org/News/2020/20200509">cuarta</a>
y <a href="https://www.debian.org/News/2020/20200801">quinta</a>
actualizaciones de su distribución «estable».
Debian 10.5 se publicó el 1 de agosto de 2020.</p>

<p><b>Debian 9 actualizado: publicada la versión 9.13</b></p>

<p>El proyecto Debian anunció las actualizaciones
<a href="https://www.debian.org/News/2019/2019090702">décima</a>,
<a href="https://www.debian.org/News/2019/20190908">undécima</a>,
<a href="https://www.debian.org/News/2020/2020020802">duodécima</a>
y <a href="https://www.debian.org/News/2020/20200718">decimotercera</a>
(y última) de su distribución «antigua estable» Debian 9
(nombre en clave <q>stretch</q>).
Debian 9.13 se publicó el 18 de julio de 2020.</p>

<p>El soporte de seguridad de la versión <q>antigua estable</q> Debian 9 se discontinuó
el 6 de julio de 2020. Sin embargo, <q>stretch</q> se beneficiará del soporte a largo plazo (LTS, por sus siglas en inglés)
<a href="https://wiki.debian.org/LTS/Stretch">hasta finales de junio de 2022</a>.
El LTS está limitado a las arquitecturas i386, amd64, armel, armhf y arm64. 
</p>

<p><b>Debian 8 LTS alcanza el final de su ciclo de vida</b></p>

<p>El 30 de junio de 2020 el soporte a largo plazo de Debian (LTS por sus siglas en inglés) para Debian 8 <q>Jessie</q> alcanzó
el final de su ciclo de vida cinco años después de su publicación inicial el 26 de abril de 2015.</p>

<p><b>Noticias sobre Debian 11 <q>bullseye</q></b></p>

<p>Paul Gevers, del equipo responsable de la publicación, <a href="https://lists.debian.org/debian-devel-announce/2020/03/msg00002.html">compartió</a>
la normativa venidera para nuestra próxima versión y las fechas tentativas de congelación junto con
otras actualizaciones y cambios. Para información adicional sobre la forma en que se gestaron
esos cambios y normativas, consulte un número anterior de los <a
href="https://lists.debian.org/debian-devel-announce/2019/07/msg00002.html">Bits del equipo responsable de la publicación</a>.</p>

<p>Aurelien Jarno propuso la
<a href="https://lists.debian.org/debian-devel-announce/2019/08/msg00003.html">eliminación de la arquitectura mips</a>
para la próxima versión porque el esfuerzo que supone su adaptación está siendo cada vez mayor.
Esta eliminación no afecta a <q>stretch</q> ni a <q>buster</q>.
</p>

<p>A la vista de la eliminación de Python 2 de <q>bullseye</q>, el equipo
debian-python proporciona información acerca del <a
href="https://lists.debian.org/debian-devel-announce/2019/11/msg00000.html">progreso y próximos pasos</a>
del proceso. Hay más detalles disponibles en su <a
href="https://wiki.debian.org/Python/2Removal">página wiki específica</a>.</p>

<p>La versión Alpha 1 del instalador de Debian Bullseye se
<a href="https://www.debian.org/devel/debian-installer/News/2019/20191205.html">publicó</a>
el 5 de diciembre de 2019.</p>

<p>Cyril Brulebois compartió información sobre la <a
href="https://lists.debian.org/debian-devel-announce/2020/03/msg00005.html">versión Alpha 2 del instalador de Debian Bullseye</a>,
publicada el 16 de marzo de 2020, que presenta muchas mejoras y cambios en
clock-setup, grub-installer, preseed y systemd, entre otros avances. Las pruebas e
informes para localizar fallos y seguir mejorando el instalador son bienvenidos. Las imágenes del instalador
y el resto de elementos relacionados están
<a href="https://www.debian.org/devel/debian-installer">disponibles para descargar</a>.</p>

<p><b>Nuevo paquete para traducción de las páginas de manual a otros idiomas</b></p>

<p>Históricamente, las páginas de manual de un paquete dado han tenido traducciones
proporcionadas en otros paquetes utilizando distintos métodos, por diferentes equipos y con formatos variados.
En la actualidad, la mayoría carecen de desarrollo activo. Antes, la mayor parte de las páginas de manual
críticas se traducían para mantenerlas actualizadas.</p>

<p>Debian publicará un nuevo paquete, 
<a href="https://lists.debian.org/debian-l10n-french/2020/02/msg00105.html">manpages-l10n</a>,
que sustituirá a Perkamon, manpages-de, manpages-fr, manpages-fr-extra, 
manpages-pl, manpages-pl-dev, manpages-pt y manpages-pt-dev. Además, 
este nuevo paquete incorpora traducciones de páginas de manual a holandés y rumano.

El resultado será que los hablantes de francés, alemán, holandés, polaco,
rumano y portugués tendrán, posiblemente, páginas de manual traducidas contenidas en
un único paquete. Los hablantes de esos idiomas podrán ayudar o informar de fallos en las
traducciones. Damos la bienvenida a quienes quieran unirse a esta tarea, pero, por favor, tenga en cuenta las áreas
con paquetes sin desarrollo activo, ya que esos paquetes tienen mayor prioridad
para su conversión a ficheros PO y posterior actualización.</p>

<p><b>Claves DKIM</b></p>

<p>Adam D. Barratt compartió información sobre una mejora reciente en la infraestructura
de Debian que permite que los DD utilicen
<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00004.html">claves DKIM para autenticación
de correos</a>.</p>

<p><b>Cambios de autenticación en Salsa</b></p>

<p>Enrico Zini anunció que <a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00007.html">las credenciales de Salsa están habilitadas en nm.debian.org</a>.
Este cambio mejora la facilidad de uso para aspirantes, responsables de paquetes y nuevos desarrolladores.
Tras este cambio, Bastian Blank, del equipo de administradores de Salsa, anunció una
<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00010.html">actualización en Salsa: eliminación de cuentas -guest</a>,
dirigida al uso de Salsa como proveedor de autenticación.</p>

<p>Sean Whitton publicó el <a href="https://www.debian.org/doc/debian-policy/">manual de normas de Debian 4.5.0.2</a>
con actualizaciones para nombres de usuario generados, uso de scripts de unidades de servicio y uso de
update-rc.d.</p>


<p><b>«Bits» del comité técnico</b></p>

<p>Mientras preparaba su charla para la DebConf20 titulada
<a href="https://debconf20.debconf.org/talks/16-meet-the-technical-committee/">Meet the Technical Committee</a>, el comité técnico publicó su
<a href="https://lists.debian.org/debian-devel-announce/2020/08/msg00005.html">informe anual</a>
y un <a href="https://salsa.debian.org/debian/tech-ctte/-/blob/master/talks/rethinking-the-tc.md">documento con propuestas</a>
para mejorar el trabajo y los procesos del comité.
</p>

<p><b>Nuevo equipo Academia de Debian («Debian Academy Team»)</b></p>

<p>
Una nueva iniciativa para definir y poner en marcha una plataforma de aprendizaje electrónico («e-learning») con
cursos específicos de Debian. ¿Quiere ayudar a hacerla realidad? Visite la
<a href="https://wiki.debian.org/DebianAcademy">página wiki</a>
y únase al equipo.
</p>

<toc-add-entry name="external">Noticias externas</toc-add-entry>

<p><b>Debian France ha renovado su junta</b></p>

<p>Debian France es una organización sin ánimo de lucro que forma parte de las «Trusted Organisations» («Organizaciones de confianza»).
Es la principal organización de confianza en Europa. Acaba de renovar su
junta. Ha habido pocos cambios, entre ellos el cargo de presidente, que pasa a ocupar Jean-Philippe MENGUAL.</p>

<p>Tenga en cuenta que en la junta de esta organización sin ánimo de lucro los cargos de presidente,
secretario y tesorero son siempre desempeñados por desarrolladores de Debian. El resto de administradores no
son todos desarrolladores de Debian, pero contribuyen a Debian en stands u otro tipo de eventos.
<a href="https://france.debian.net/posts/2020/renouvellement_instances/">Consulte la noticia</a>.
</p>


<toc-add-entry name="MiniDebs">MiniDebConfs, MiniDebCamps y DebConf20</toc-add-entry>

<p><b>MiniDebConfs and MiniDebCamps</b></p>
<p>
A comienzos de 2020 había siete <a href="https://wiki.debian.org/MiniDebConf">MiniDebConfs</a>
programadas alrededor del mundo, lo que reflejaba la vitalidad de nuestra comunidad.
Organizadas por miembros del proyecto Debian, las MiniDebConfs están abiertas a todo el mundo y
proporcionan una oportunidad para que desarrolladores, contribuidores y otras personas interesadas
se encuentren o se conozcan en personas. Sin embargo, debido a las restricciones impuestas como consecuencia de la pandemia del
coronavirus (COVID-19), la mayoría de esas conferencias, si no todas, se han cancelado o se han
pospuesto a 2021: 
<a href="https://maceio2020.debian.net">Maceió (Brasil)</a>,
<a href="https://wiki.debian.org/DebianEvents/gb/2020/MiniDebConfAberdeen">Aberdeen (Escocia)</a>,
<a href="https://wiki.debian.org/DebianEvents/fr/2020/Bordeaux">Burdeos (Francia)</a>,
<a href="https://wiki.debian.org/DebianLatinoamerica/2020/MiniDebConfLatAm">El Salvador</a> y
<a href="https://lists.debian.org/debian-devel-announce/2020/02/msg00006.html">Ratisbona (Alemania)</a>.
Algunas se ha reprogramado como conferencias en línea: la primera,
<a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline">MiniDeconfOnline #1</a>,
precedida por un DebCamp, se celebró del 28 al 31 de mayo de 2020. La 
<a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline2">MiniDebConf Online #2 "Gaming Edition"</a>,
dedicada a los juegos en Debian (y en Linux en general), tendrá lugar entre el 19
y el 22 de noviembre de 2020 - siendo los dos primeros días un Debcamp. Visite la
<a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline2">página wiki del evento</a>
para más detalles.</p>


<p><b>DebConf20 celebrada en línea, DebConf21 tendrá lugar en Haifa, Israel</b></p>

<p>
<a href="https://www.debian.org/News/2020/20200830">DebConf20</a> ha sido
la primera DebConf que se ha celebrado en línea, debido a la pandemia de enfermedad por el coronavirus 
(COVID-19).</p>

<p>La comunidad de Debian se ha adaptado a este cambio para seguir adelante con la compartición
de ideas, reuniones informales (BoF, por sus siglas en inglés: «Birds of a Feather»), debates y todas las
actividades que se han convertido en tradiciones a lo largo de los años. Todas las sesiones se
retransmitieron y contaron con diferentes formas de participación: a través de mensajería IRC,
de documentos de texto colaborativos y de salas de reuniones virtuales.
</p> 

<p>Con más de 850 participantes de 80 países y un total de más de
100 charlas, debates, reuniones informales (BoF) y
otras actividades, la <a href="https://debconf20.debconf.org">DebConf20</a> fue un
gran éxito. La programación de la DebConf20 incluía dos canales de habla
no inglesa: la MiniConf en español y la MiniConf en malabar.</p>

<p>La mayoría de las charlas y sesiones están disponibles en el <a
href="https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/">archivo web de encuentros Debian</a>
y el <a href="https://debconf20.debconf.org/">sitio web DebConf20</a>
permanecerá activo como archivo y continuará ofreciendo enlaces a las
presentaciones y vídeos de charlas y eventos.
</p>

<p>
Para el próximo año está prevista la celebración de la <a href="https://wiki.debian.org/DebConf/21">DebConf21</a>
en Haifa, Israel, en agosto o septiembre.
</p>

<toc-add-entry name="debday">Día de Debian</toc-add-entry>

<p>¡Fuerte 27 años! ¡Feliz aniversario! ¡Feliz <a href="https://wiki.debian.org/DebianDay/2020">#DebianDay!</a>
Debido a la pandemia mundial Covid-19, se organizaron menos eventos locales para el día de Debian
y algunos de ellos se celebraron online en <a href="https://wiki.debian.org/Brasil/Eventos/DebianDayBrasil2020">Brasil</a>
(15 a 16 de agosto). No obstante, desarrolladores y contribuidores tuvieron la oportunidad
de encontrarse en La Paz (Bolivia), České Budějovice (República Checa) y Haifa
(Israel) durante la DebConf20.
</p>

<toc-add-entry name="otherevents">Otros eventos</toc-add-entry>

<p>
La comunidad de Debian Brasil organizó entre el 3 de mayo y el 6 de junio de 2020 un evento
en línea llamado
<a href="https://debianbrasil.gitlab.io/FiqueEmCasaUseDebian/">#FiqueEmCasaUseDebian (#QuedateEnCasaUsaDebian)</a>.
Durante 27 noches, los DD Daniel Lenharo y Paulo Santana (phls) recibieron a invitados
que compartieron su conocimiento sobre Debian. Puede leer un
<a href="http://softwarelivre.org/debianbrasil/blog/fiqueemcasausedebian-it-was-35-days-with-talks-translations-and-packaging">informe</a>.
</p>

<toc-add-entry name="reports">Informes</toc-add-entry>
## It's easier to link to the monthly reports for the LTS section and the RB links rather than
# summarize each report or number of packages. Feel free to input this information if you need to fill
# the issue out
#

<p><b>Informes mensuales de Freexian sobre LTS</b></p>

<p>Freexian publica <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">informes mensuales</a>
sobre el trabajo de los contribuidores remunerados para el soporte a largo plazo de Debian.
</p>

<p><b>Estado actual de Compilaciones reproducibles</b></p>

<p>Siga en el <a
href="https://reproducible-builds.org/blog/">blog de
Compilaciones reproducibles</a> los informes mensuales de su trabajo en el ciclo de vida de <q>Buster</q>.
</p>


<toc-add-entry name="help">Se necesita ayuda</toc-add-entry>
#Make bold -fix
<p><b>Equipos que necesitan ayuda</b></p>
## Teams needing help
## $Link to the email from $Team requesting help

<p>El manual Referencia del desarrollador de Debian ahora se mantiene como ReStructuredText. Son bienvenidas
traducciones y actualizaciones de las traducciones existentes - consulte el anuncio en las
<a href="https://lists.debian.org/debian-devel-announce/2019/08/msg00003.html">noticias varias para desarrolladores</a> y
el hilo <a href="https://lists.debian.org/debian-devel/2020/02/msg00500.html">traducciones de developers-reference</a>
para más información.</p>

<p><b>Apertura del plazo de recepción de propuestas de obras artísticas para bullseye</b></p>

<p>Jonathan Carter hizo la <a
href="https://lists.debian.org/debian-devel-announce/2020/08/msg00002.html">apertura oficial del plazo de recepción
de propuestas de obras artísticas para bullseye</a>.
Para detalles actualizados, consulte la <a
href="https://wiki.debian.org/DebianDesktop/Artwork/Bullseye">wiki</a>. 
Al mismo tiempo, nos gustaría agradecer a Alex Makas el diseño del <a
href="https://wiki.debian.org/DebianArt/Themes/futurePrototype">tema futurePrototype
para buster</a>. Si a usted le gustaría, o conoce a alguien a quien le gustaría,
crear un tema para escritorio («desktop look and feel»), asegúrese de enviar su obra artística. La fecha
límite es el 10 de octubre de 2020.</p>

<p><b>Paquetes que necesitan ayuda</b></p>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="https://lists.debian.org/debian-devel/2020/09/msg00058.html"
        orphaned="1191"
        rfa="213" />

<p><b>Fallos para principiantes</b></p>

## check https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer and add outstanding + forwarded + pending upload
<p>
Debian tiene una etiqueta de clasificación de fallos llamada <q>newcomer</q> («principiante»), usada para marcar fallos adecuados para que los nuevos
contribuidores los utilicen como punto de entrada para trabajar en paquetes específicos.

En la actualidad hay <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">195</a>
fallos disponibles marcados <q>newcomer</q>.
</p>

<toc-add-entry name="code">Programas, programadores y contribuidores</toc-add-entry>
<p><b>Nuevos y nuevas responsables de paquetes desde el 1 de julio de 2019</b></p>
##Run the ./new-package-maintainers script locally
##./new-package-maintainers YYYMMDD
<p>
Damos la bienvenida a: Marie Alexandre, Saira Hussain, laokz, Arthur Diniz, Daniel
Pimentel, Ricardo Fantin da Costa, Ivan Noleto, Joao Seckler, Andre Marcelo
Alvarenga, Felipe Amorim, Pedro Vaz de Mello de Medeiros, Marco Villegas,
Thiago Gomes Verissimo, Teemu Toivola, Giuliano Augusto Faulin Belinassi,
Miguel Figueiredo,  Stephan Lachnit, Nicola Di Lieto, Ximin Luo, Paul Grosu,
Thomas Ward, Ganael Laplanche, Aaron M. Ucko, Rodrigo Carvalho, Lukas
Puehringer, Markus Teich, Alexander Ponyatykh, Rob Savoury, Joaquin de Andres,
Georges Basile Stavracas Neto, Jian-Ding Chen (timchen119), Juan Picca,
Katerina, Matthew Fernandez, Shane McDonald, Eric Desrochers, Remi Duraffort,
Sakirnth Nagarasa, Ambady Anand S., Abhijith Sheheer, Helen Koike, Sven Hartge,
Priyanka Saggu, Sebastian Holtermann, Jamie Bliss, David Hart, James Tocknell,
Julien Schueller, Matt Hsiao, Jafar Al-Gharaibeh, Matthias Blümel, Dustin
Kirkland, Gao Xiang , Alberto Leiva Popper, Benjamin Hof, Antonio Russo, Jérôme
Lebleu, Ramon Fried, Evangelos Rigas, Adam Cecile, Martin Habovstiak, Lucas
Kanashiro, Alessandro Grassi, Estebanb, James Price, Cyril Richard, John Scott,
David Bürgin, Beowulf, Igor Petruk, Thomas Dettbarn, Vifly, Lajos Veres,
Andrzej Urbaniak, Phil Wyett, Christian Barcenas, Johannes Schilling, Josh
Steadmon, Sven Hesse, Gert Wollny, suman rajan, kokoye2007, Kei Okada, Jonathan
Tan, David Rodriguez, David Krauser, Norbert Schlia, Pranav Ballaney, Steve
Meliza, Fabian Grünbichler, Sao I Kuan, Will Thompson, Abraham Raji, Andre
Moreira Magalhaes, Lorenzo Puliti, Dmitry Baryshkov, Leon Marz, Ryan Pavlik,
William Desportes, Michael Elterman, Simon, Schmeisser, Fabrice Bauzac, Pierre
Gruet, Mattia Biondi, Taowa Munene-Tardif, Sepi Gair, Piper McCorkle, Alois
Micard, xiao sheng wen, Roman Ondráček, Abhinav Krishna C K, Konstantin Demin,
Pablo Mestre Drake, Harley Swick, Robin Gustafsson, Hamid Nassiby, Étienne
Mollier, Karthik, Ben Fiedler, Jair Reis, Jordi Sayol, Emily Shaffer, Fabio dos
Santos Mendes, Bruno Naibert de Campos, Junior Figueredo, Marcelo Vinicius
Campos Amedi, Tiago Henrique Vercosa de Lima, Deivite Huender Ribeiro Cardoso,
Guilherme de Paula Xavier Segundo, Celio Roberto Pereira, Tiago Rocha, Leandro
Ramos, Filipi Souza, Leonardo Santiago Sidon da Rocha, Asael da Silva Vaz,
Regis Fernandes Gontijo, Carlos Henrique Lima Melara, Alex Rosch de Faria, Luis
Paulo Linares, Jose Nicodemos Vitoriano de Oliveira, Marcio Demetrio Bacci,
Fabio Augusto De Muzio Tobich, Natan Mourao, Wilkens Lenon, Paulo Farias,
Leonardo Rodrigues Pereira, Elimar Henrique da Silva, Delci Silva Junior, Paulo
Henrique Hebling Correa, Gleisson Jesuino Joaquim Cardoso, Joao Paulo Lima de
Oliveira, Jesus Ali Rios, Jaitony de Sousa, Leonardo Santos, Aristo Chen,
Olivier Humbert, Roberto De Oliveira, Martyn Welch, Arun Kumar Pariyar, Vasyl
Gello, Antoine Latter, Ken VanDine, Francisco M Neto, Dhavan Vaidya, Raphael
Medaer, Evangelos Ribeiro Tzaras, Richard Hansen, Joachim Langenbach, Jason
Hedden, Boian Bonev, Kay Thriemer, Sławomir Wójcik, Lukas Märdian, Kevin Duan,
zhao feng, Leandro Cunha, Cocoa, J0J0 T, Johannes Tiefenbacher, Iain Parris,
Guobang Bi, Lasse Flygenring-Harrsen, Emanuel Krivoy, Qianqian Fang, Rafael
Onetta Araujo, Shruti Sridhar, Alvin Chen, Brian Murray, Nick Gasson.
</p>

<p><b>Nuevos mantenedores y mantenedoras de Debian</b></p>
##Run the ./dd-dm-from-keyring.pl script locally for new DM and DD
##./dd-dm-from-keyring.pl YYYMMDD
<p>
Damos la bienvenida a: Hans van Kranenburg, Scarlett Moore, Nikos Tsipinakis, Joan
Lledó, Baptiste Beauplat, Jianfeng Li, Denis Danilov, Joachim Falk, Thomas
Perret, William Grzybowski, Lars Tangvald, Alberto Molina Coballes, Emmanuel
Arias, Hsieh-Tseng Shen, Jamie Strandboge, Sven Geuer, Håvard Flaget Aasen,
Marco Trevisan, Dennis Braun, Stephane Neveu, Seunghun Han, Alexander Johan
Georg Kjäll, Friedrich Beckmann, Diego M. Rodriguez, Nilesh Patra, Hiroshi
Yokota, Shayan Doust, Chirayu Desai, Arnaud Ferraris, Francisco Vilmar Cardoso
Ruviaro, Patrick Franz, François Mazen, Kartik Kulkarni, Fritz Reichwald, Nick
Black, Octavio Alvarez.
</p>

<p><b>Nuevos desarrolladores y desarrolladoras de Debian</b></p>
<p>
Damos la bienvenida a: Keng-Yu Lin, Judit Foglszinger, Teus Benschop, Nick Morrott,
Ondřej Kobližek, Clément Hermann, Gordon Ball, Louis-Philippe Véronneau, Olek
Wojnar, Sven Eckelmann, Utkarsh Gupta, Robert Haist, Gard Spreemann, Jonathan
Bustillos, Scott Talbert, Paride Legovini, Ana Custura, Felix Lechner, Richard
Laager, Thiago Andrade Marques, Vincent Prat, Michael Robin Crusoe, Jordan
Justen, Anuradha Weeraman, Bernelle Verster, Gabriel F. T. Gomes, Kurt
Kremitzki, Nicolas Mora, Birger Schacht and Sudip Mukherjee.
</p>

<p><b>Contribuidores y contribuidoras</b></p>

## Visit the link below and pull the information manually.

<p>
En la página de <a href="https://contributors.debian.org/">contribuidores y contribuidoras a Debian</a> de
2020 hay 1375 personas y 9 equipos listados en la actualidad.
</p>


<p><b>Paquetes nuevos y de interés</b></p>

<p>
Una muestra de los muchos paquetes <a href="https://packages.debian.org/unstable/main/newpkg">
añadidos al archivo de Debian «inestable»</a> en las últimas semanas:</p>

<ul>
<li><a href="https://packages.debian.org/unstable/main/bpftool">bpftool - Inspección y manipulación simple de programas y mapas BPF</a></li>
<li><a href="https://packages.debian.org/unstable/main/elpa-magit-forge">elpa-magit-forge - Trabaje con forjas Git desde la comodidad de Magit</a></li>
<li><a href="https://packages.debian.org/unstable/main/libasmjit0">libasmjit0 - Ensamblador x86/x64 JIT y AOT completo para C++</a></li>
<li><a href="https://packages.debian.org/sid/libraritan-rpc-perl">libraritan-rpc-perl - Módulo Perl para la interfaz Raritan JSON-RPC</a></li>
<li><a href="https://packages.debian.org/unstable/main/python3-rows">python3-rows - Biblioteca para tabular datos, no importa el formato</a></li>
</ul>


<p><b>Érase una vez en Debian</b></p>

## Items pulled from the Timeline https://timeline.debian.net
## Jump to any random year/ same month/ same week.
## Provide link and link description.
## This may work better with a script at some point, but for now let us see
## what the ease of work is.

## Format - YYYY-MM-DD text

<ul>

<li>2007-09-02 <a href="https://lists.debian.org/debian-devel-announce/2007/09/msg00001.html">packages.debian.org actualizado</a></li>

<li>2006-09-04 <a href="https://lists.debian.org/debian-devel-announce/2006/09/msg00002.html">cdrkit (bifurcación, o «fork», de cdrtools) subido</a></li>

<li>2009-09-04 <a href="https://lists.debian.org/debian-devel-announce/2009/09/msg00002.html">ahora el paquete grub se basa en GRUB2</a></li>

<li>1997-09-06 <a href="https://lists.debian.org/debian-devel-announce/1997/09/msg00000.html">implementado el seguimiento de la gravedad («severity») en el BTS</a></li>

<li>2008-09-08 <a href="https://lists.debian.org/debian-devel-announce/2008/09/msg00001.html">la arquitectura m68k se traslada a debian-ports.org</a></li>

</ul>


<toc-add-entry name="continuedpn">¿Quiere seguir leyendo DPN?</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">Suscríbase o cancele su suscripción</a> a la lista de correo de Noticias Debian.</p>

#use wml::debian::projectnews::footer editor="el equipo de publicidad («Publicity Team») con contribuciones de Laura Arjona Reina, Jean-Pierre Giraud, Paulo Henrique de Lima Santana, Jean-Philippe Mengual, Donald Norwood"
# No need for the word 'and' for the last contributor, it is added at build time.
# Please add the contributors to the /dpn/CREDITS file
# Translators may also add a translator="foo, bar, baz" to the previous line
