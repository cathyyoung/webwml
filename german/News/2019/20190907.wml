<define-tag pagetitle>Debian 10 aktualisiert: 10.1 veröffentlicht</define-tag>
<define-tag release_date>2019-09-07</define-tag>
#use wml::debian::news
# $Id:
#use wml::debian::translation-check translation="21882a152ab072f844cf526dc172ff70559e05e7" maintainer="Erik Pfannenstein"


<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die erste Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Stable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können. 
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser 
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter: 
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction acme-tiny "Vorbereitung auf die anstehenden ACME-Protokolländerungen">
<correction android-sdk-meta "Neue Veröffentlichung der Originalautoren; regulären Ausdruck zum Hinzufügen der Debian-Version zu Binärpaketen korrigiert">
<correction apt-setup "Vorbefüllen (preseeding) von Secure Apt für lokale Paketdepots via apt-setup/localX/ überarbeitet">
<correction asterisk "Pufferüberlauf in res_pjsip_messaging [AST-2019-002 / CVE-2019-12827] behoben; Anfälligkeit für Absturz aus der Ferne in chan_sip [AST-2019-003 / CVE-2019-13161] behoben">
<correction babeltrace "ctf-symbols-Abhängigkeiten auf Nach-Merge-Version abgeändert">
<correction backup-manager "Leeren von entfernten Archiven via FTP oder SSH überarbeitet">
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung">
<correction basez "base64url-kodierte Zeichenketten richtig dekodieren">
<correction bro "Sicherheitsprobleme [CVE-2018-16807 CVE-2018-17019]">
<correction bzip2 "Regression beim Entpacken einiger Dateien behoben">
<correction cacti "Probleme beim Upgraden von der Stretch-Version behoben">
<correction calamares-settings-debian "Berechtigungen fürs initramfs-Abbild für den Fall korrigiert, dass Festplatten-Vollverschlüsselung aktiv ist [CVE-2019-13179]">
<correction ceph "Neubau gegen neue libbabeltrace">
<correction clamav "Extraktion nichtrekursiver ZIP-Bomben verhindern; neue Version der Originalautoren mit Sicherheits-Korrekturen - Scan-Zeit-Bregrenzung als Maßnahme gegen ZIP-Bomben [CVE-2019-12625]; Schreibvorgang außerhalb der Grenzen in der NSIS-bzip2-Bibliothek behoben [CVE-2019-12900]">
<correction cloudkitty "Baufehlschläge mit aktualisiertem SQLAlchemy behoben">
<correction console-setup "Internationalisierungs-Probleme beim Umschalten der Locales mit Perl &gt;= 5.28 behoben">
<correction cryptsetup "Unterstützung für LUKS2-Header ohne angebundenen Keyslot behoben; Überlaufen gemappter Segmente auf 32-Bit-Architekturen behoben">
<correction cups "Mehrere Sicherheits-/Offenlegungsprobleme behoben - SNMP-Pufferüberläufe [CVE-2019-8696 CVE-2019-8675], IPP-Pufferüberlauf, Dienstblockade- und Speicheroffenlegungsprobleme im Scheduler">
<correction dbconfig-common "Problem behoben, das durch Änderungen in bashs POSIX-Verhalten verursacht wurde">
<correction debian-edu-config "Beim LTSP-Client-Boot die PXE-Option <q>ipappend 2</q> verwenden; sudo-ldap-Konfiguration überarbeitet; Verlust dynamisch zugewiesener IPv4-Adressen behoben; mehrere Korrekturen und Verbesserungen an debian-edu-config.fetch-ldap-cert">
<correction debian-edu-doc "Debian Edu Buster- und ITIL-Handbücher und -Übersetzungen überarbeitet">
<correction dehydrated "Abruf von Konten-Informationen überarbeitet; Nachfolge-Korrekturen für den Umgang mit Konten-IDs und APIv1-Kompatiblität">
<correction devscripts "debchange: mit Option --bpo buster-backports anpeilen">
<correction dma "TLS-Verbindungen nicht auf TLS 1.0 beschränken">
<correction dpdk "Neue stabile Veröffentlichung der Originalautoren">
<correction dput-ng "Codenamen für buster-backports und stretch-backports-sloppy hinzugefügt">
<correction e2fsprogs "e4defrag-Abstürze auf 32-Bit-Architekturen behoben">
<correction enigmail "Neue Version der Originalautoren, Sicherheitskorrekturen [CVE-2019-12269]">
<correction epiphany-browser "Sicherstellen, dass die Web-Erweiterung die mitgelieferte Kopie der libdazzle verwendet">
<correction erlang-p1-pkix "Umgang mit GnuTLS-Zertifkaten überarbeitet">
<correction facter "Auswertung von Linux-Route-non-key-/Value-Schaltern (z. B. onlink) überarbeitet">
<correction fdroidserver "Neue Veröffentlichung der Originalautoren">
<correction fig2dev "Bei Kreis-/Halbkreis-Pfeilköpfen nicht mit Speicherzugriffsfehler abstürzen, wenn die Vergrößerung stärker als 42 ist [CVE-2019-14275]">
<correction firmware-nonfree "atheros: Qualcomm Atheros QCA9377 rev 1.0 Firmware-Version WLAN.TF.2.1-00021-QCARMSWP-1 hinzugefügt; realtek: Realtek RTL8822CU Bluetooth-Firmware hinzugefügt; atheros: Änderung an QCA9377 rev 1.0-Firmware in 20180518-1 zurückgenommen; misc-nonfree: Firmware für MediaTek MT76x0/MT76x2u-Drahtlos-Chips, MediaTek MT7622/MT7668-Bluetooth-Chips und signierte GV100-Firmware hinzugefügt">
<correction freeorion "Absturz beim Laden oder Speichern von Spieldaten behoben">
<correction fuse-emulator "Das X11-Backend dem Wayland-Backend vorziehen; Fuse-Symbol auf dem GTK-Fenster und im Info-Dialog anzeigen">
<correction fusiondirectory "Strengere Prüfungen bei LDAP-Abfragen; fehlende Abhängigkeit von php-xml hinzugefügt">
<correction gcab "Korrumption beim Extrahieren behoben">
<correction gdb "Neubau gegen neues libbabeltrace">
<correction glib2.0 "GKeyFile-Einstellungs-Backend soll ~/.config und Konfigurationsdateien strenge Berechtigungen geben [CVE-2019-13012]">
<correction gnome-bluetooth "GNOME-Shell-Abstürze beim Verwenden von gnome-shell-extension-bluetooth-quick-connect behoben">
<correction gnome-control-center "Absturz beim Auswählen von Details -&gt; Übersicht (info-overview) behoben; Speicherlecks im Barrierefreiheits-Panel behoben; Regression behoben, die dazu geführt hat, dass in der Bildschirmvergrößerung die Mausverfolgung nicht funktionierte; Isländisch- und Japanisch-Übersetzungen aktualisiert">
<correction gnupg2 "Viele Fehlerbehebungen und Stabilitätsverbesserungen von den Originalautoren zurückportiert; keys.openpgp.org als Standard-Schlüsselserver benutzen; standardmäßig nur Selbst-Signaturen importieren">
<correction gnuplot "Unvollständige/unsichere Initialisierung des ARGV-Arrays behoben">
<correction gosa "Strengere Prüfungen der LDAP-Abfragen">
<correction hfst "Reibungsärmere Upgrades aus Stretch sichergestellt">
<correction initramfs-tools "Ruhezustand-Fortsetzen abschalten, wenn keine passenden Swap-Geräte vorhanden sind; MODULES=most: alle Tastaturtreibermodule, cros_ec_spi und SPI-Treiber, extcon-usbc-cros-ec integrieren; MODULES=dep: extcon-Treiber mitliefern">
<correction jython "Rückwärts-Kompatibilität mit Java 7 beibehalten">
<correction lacme "Aktualisierung, mit der die Unterstützung von unauthentifizierten GET-Abfragen aus der Let's Encrypt ACMEv2-API entfernt wird">
<correction libblockdev "Bestehende cryptsetup-API zum Ändern der Keyslot-Passphrase benutzen">
<correction libdatetime-timezone-perl "Aktualisierung für die enthaltenen Daten">
<correction libjavascript-beautifier-perl "Unterstützung für den <q>=&gt;</q>-Operator hinzugefügt">
<correction libsdl2-image "Pufferüberläufe behoben [CVE-2019-5058 CVE-2019-5052 CVE-2019-7635]; Zugriff außerhalb der Grenzen in der PCX-Handhabung behoben [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction libtk-img "Aufhören, interne Kopien der JPEG-, Zlib- und PixarLog-Codecs zu benutzen und deswegen abzustürzen">
<correction libxslt "Umgehung des Sicherheits-Rahmenwerks behoben [CVE-2019-11068], ebenso das nichtinitialisierte Lesen des xsl:number-Tokens [CVE-2019-13117] und uninitialisiertes Lesen mit UTF-8 Gruppierungszeichen [CVE-2019-13118]">
<correction linux "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-latest "Aktualisierung für das 4.19.0-6-Kernel-ABI">
<correction linux-signed-amd64 "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-signed-arm64 "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-signed-i386 "Neue stabile Veröffentlichung der Originalautoren">
<correction lttv "Neubau gegen das neue libbabeltrace">
<correction mapproxy "WMS-Capabilities mit Python 3.7 überarbeitet">
<correction mariadb-10.3 "Neue stabile Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2019-2737 CVE-2019-2739 CVE-2019-2740 CVE-2019-2758 CVE-2019-2805]; Speicherzugriffsfehler beim Zugriff auf das 'information_schema' behoben; 'mariadbcheck' zu 'mariadb-check' umbenannt">
<correction musescore "Webkit-Funktionalität abgeschaltet">
<correction ncbi-tools6 "Ohne die unfreien data/UniVec.* neu paketiert">
<correction ncurses "<q>rep</q> aus xterm-new und den abgeleiteten terminfo-Beschreibungen entfernt">
<correction netdata "Google Analytics aus der erzeugten Dokumentation entfernt; dem Versand anonymer Statistiken widersprochen; <q>Sign in</q>-Button entfernt">
<correction newsboat "Weiternenutzung nach Freigabe (use after free) behoben">
<correction nextcloud-desktop "Fehlende Abhängigkeit von nextcloud-desktop-common in nextcloud-desktop-cmd hinzugefügt">
<correction node-lodash "Prototype-Pollution-Problem behoben [CVE-2019-10744]">
<correction node-mixin-deep "Prototype-Pollution-Problem behoben">
<correction nss "Sicherheitsprobleme behoben [CVE-2019-11719 CVE-2019-11727 CVE-2019-11729]">
<correction nx-libs "Eine Anzahl Speicherlecks behoben">
<correction open-infrastructure-compute-tools "Container-Start überarbeitet">
<correction open-vm-tools "Richtig mit Betriebssystem-Versionsnummern der Form <q>X</q> statt <q>X.Y</q> umgehen">
<correction openldap "rootDN proxyauthz auf seine eigenen Datenbanken beschränken [CVE-2019-13057]; sasl_ssf-ACL-Statement bei jeder Verbindung erzwingen [CVE-2019-13565]; slapo-rwm überarbeitet, damit es den Originalfilter nicht überschreibt, wenn der korrigierte Filter ungültig ist">
<correction osinfo-db "Informationen über Buster 10.0 hinzugefügt; URLs des Stretch-Downloads korrigiert; Namen des Parameters, der beim Erzeugen einer Vorbefüllungsdatei (preseed file) den vollen Namen enthält, korrigiert">
<correction osmpbf "Neukompilierung mit protobuf 3.6.1">
<correction pam-u2f "Unsichere Handhabung von Debugging-Dateien überarbeitet [CVE-2019-12209]; Debugdateien-Deskriptorleck behoben [CVE-2019-12210]; Zugriff außerhalb der Grenzen behoben; Speicherzugriffsfehler beseitigt, der auftritt, wenn kein Puffer alloziert werden kann">
<correction passwordsafe "Lokalisierungsdateien ins richtige Verzeichnis installieren">
<correction piuparts "Konfigurationen für Buster-Veröffentlichung aktualisiert; fadenscheinigen Fehlschlag beim Entfernen von Paketen, deren Namen mit '+' enden, korrigiert; separate Tarball-Namen für --merged-usr-Chroots erzeugen">
<correction postgresql-common "<q>pg_upgradecluster von postgresql-common 200, 200+deb10u1, 201 und 202 beschädigen die data_directory-Einstellung, wenn sie *zwei Mal* benutzt werden, um ein Cluster upzugraden (z. B. 9.6 -&gt; 10 -&gt; 11)</q> behoben">
<correction pulseaudio "Stummschaltungen wiederherstellen">
<correction puppet-module-cinder "Nicht versuchen, in /etc/init zu schreiben">
<correction python-autobahn "pyqrcode Kompilierungs-Abhängigkeiten überarbeitet">
<correction python-django "Neue Sicherheitsveröffentlichung der Originalautoren [CVE-2019-12781]">
<correction raspi3-firmware "Unterstützung für Raspberry Pi Compute Module 3 (CM3), Raspberry Pi Compute Module 3 Lite und Raspberry Pi Compute Module IO Board V3 hinzugefügt">
<correction reportbug "Nach der Buster-Veröffentlichung die Veröffentlichungsnamen aktualisiert; stretch-pu-Anfragen wieder eingeschaltet; Abstürze beim Nachschlagen der Pakete/Versionen behoben; fehlende Abhängigkeit von sensible-utils hinzugefügt">
<correction ruby-airbrussh "Keine Ausnahme auswerfen, wenn SSH-Ausgabe ungültige UTF-8-Zeichen enthält">
<correction sdl-image1.2 "Pufferüberläufe behoben [CVE-2019-5052 CVE-2019-7635], ebenso Zugriff außerhalb der Grenzen [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction sendmail "sendmail-bin.postinst, initscript: start-stop-daemon soll PID-Datei und ausführbare Datei finden; sendmail-bin.prerm: Sendmail vor dem Entfernen der Alternativen anhalten">
<correction slirp4netns "Neue stabile Veröffentlichung der Originalautoren mit Sicherheitskorrekturen - sscanf-Ergebnis überprüfen, wenn ident emuliert wird [CVE-2019-9824]; Heap-Überlauf im enthaltenen libslirp beseitigt [CVE-2019-14378]">
<correction systemd "Netzwerk: Fehlschläge beim Aktivieren der Schnittstelle auf dem Linux-Kernel 5.2 behoben; ask-password: Pufferüberlauf beim Lesen des Schlüsselbundes verhindert; Netzwerk: weniger rappeln, wenn IPv6 abgeschaltet ist">
<correction tzdata "Neue Aktualisierung der Originalautoren">
<correction unzip "ZIP-Bomben-Probleme behoben [CVE-2019-13232]">
<correction usb.ids "Routine-Aktualisierung der USB-IDs">
<correction warzone2100 "Speicherzugriffsfehler beim Veranstalten einer Mehrspielerpartie behoben">
<correction webkit2gtk "Neue stabile Version der Originalautoren; keine SSE2-fähigen CPUs mehr voraussetzen">
<correction win32-loader "Neukompilierung gegen aktuelle Pakete, vor allem debian-archive-keyring; Kompilierungsfehlschlag durch Erzwingen einer POSIX-Locale behoben">
<correction xymon "Mehrere (serverbezogene) Sicherheitsprobleme behoben [CVE-2019-13273 CVE-2019-13274 CVE-2019-13451 CVE-2019-13452 CVE-2019-13455 CVE-2019-13484 CVE-2019-13485 CVE-2019-13486]">
<correction yubikey-personalization "Zusätzliche sicherheitstechnische Vorsichtsmaßnahmen zurückportiert">
<correction z3 "Den SONAME von libz3java.so nicht auf libz3.so.4 setzen">
</table>


<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2019 4477 zeromq3>
<dsa 2019 4478 dosbox>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4480 redis>
<dsa 2019 4481 ruby-mini-magick>
<dsa 2019 4482 thunderbird>
<dsa 2019 4483 libreoffice>
<dsa 2019 4484 linux>
<dsa 2019 4484 linux-signed-i386>
<dsa 2019 4484 linux-signed-arm64>
<dsa 2019 4484 linux-signed-amd64>
<dsa 2019 4486 openjdk-11>
<dsa 2019 4488 exim4>
<dsa 2019 4489 patch>
<dsa 2019 4490 subversion>
<dsa 2019 4491 proftpd-dfsg>
<dsa 2019 4493 postgresql-11>
<dsa 2019 4494 kconfig>
<dsa 2019 4495 linux-signed-amd64>
<dsa 2019 4495 linux-signed-arm64>
<dsa 2019 4495 linux>
<dsa 2019 4495 linux-signed-i386>
<dsa 2019 4496 pango1.0>
<dsa 2019 4498 python-django>
<dsa 2019 4499 ghostscript>
<dsa 2019 4501 libreoffice>
<dsa 2019 4502 ffmpeg>
<dsa 2019 4503 golang-1.11>
<dsa 2019 4504 vlc>
<dsa 2019 4505 nginx>
<dsa 2019 4507 squid>
<dsa 2019 4508 h2o>
<dsa 2019 4509 apache2>
<dsa 2019 4510 dovecot>
</table>

<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction pump "Unbetreut; Sicherheitsprobleme">
<correction rustc "Veralteten rust-doc-Müll entfernt">
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühnungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt;, oder kontaktieren das 
Stable-Release-Team (auch auf Englisch) über 
&lt;debian-release@lists.debian.org&gt;.</p>

