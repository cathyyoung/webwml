<define-tag pagetitle>DebConf21 online schließt</define-tag>

<define-tag release_date>2021-08-28</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="eebf73f6f1306851590138e0b9fc3917cf4580a6" maintainer="Erik Pfannenstein"

<p>
Am Samstag, dem 28. August 2021, kam die jährliche Debian Entwickler- und
Unterstützerkonferenz zu ihrem Ende.
</p>

<p>
Aufgrund der Coronavirus-Pandemie (COVID-19) wurde auch die DebConf 21 wieder
online abgehalten. 
</p>


<p>
Alle Veranstaltungen wurden gestreamt und boten mehrere Möglichkeiten zur
Teilnahme: IRC, kollaborative Online-Textdokumente
und virtuelle Videokonferenzräume.
</p>

<p>
Mit 740 registrierten Teilnehmerinnen und Teilnehmern aus mehr als fünfzehn
verschiedenen Ländern und ingesamt 70 Vorträgen, Diskussionsrunden,
Birds-of-a-Feather-Versammlungen (BoF) und anderen Aktivitäten war die
<a href="https://debconf21.debconf.org">DebConf21</a> sehr erfolgreich.
</p>

<p>
Die Infrastruktur, die schon bei früheren Online-Veranstaltungen verwendet
wurde, war für DebConf21 angepasst worden und hat gut funktioniert. Zu
dieser Infratruktur gehören Anwendungen wie Jitsi, OBS, Voctomix, SReview,
nginx, Etherpad und eine webbasierte Oberfläche für Voctomix. Sie und alle
anderen Komponenten sind freie Software und werden durch das
<a href="https://salsa.debian.org/debconf-video-team/ansible">Ansible-</a>Depot 
des Video-Teams konfiguriert.
</p>

<p>
Der DebConf21-<a href="https://debconf21.debconf.org/schedule/">Veranstaltungskalender</a>
umfasste eine Vielzahl von Veranstaltungen, die in verschiedenen Tracks organisiert waren:
</p>
<ul>
<li>Introduction to Free Software &amp; Debian,</li>
<li>Packaging, policy, and Debian infrastructure,</li>
<li>Systems administration, automation and orchestration,</li>
<li>Cloud and containers,</li>
<li>Security,</li>
<li>Community, diversity, local outreach and social context,</li>
<li>Internationalization, Localization and Accessibility,</li>
<li>Embedded &amp; Kernel,</li>
<li>Debian Blends and Debian derived distributions,</li>
<li>Debian in Arts &amp; Science</li>
<li>und andere.</li>
</ul>

<p>
Die Vorträge wurden in zwei Räumen ausgestrahlt und viele davon wurden in
verschiedenen Sprachen wie Telugu, Portugiesisch, Malayalam, Kannada, Hindi,
Marathi und Englisch gehalten, sodass ein größeres Publikum daran teilhaben konnte.
</p>

<p>
Zwischen den Vorträgen wurden neben der Schleife mit den üblichen Sponsoren
auch einige Videoclips mit Fotos früherer DebConfs gezeigt. Außerdem gab es
Spaßfakten über Debian und kurze Grußvideos von Teilnehmern mit Nachrichten
an ihre Debian-Freunde.
</p>

<p>Das Debian Publicity-Team betrieb die übliche »Live-Berichterstattung«.
Micronews, welche die verschiedenen Veranstaltungen ankündigten, steigerten
die Partizipation. Außerdem stellte das DebConf-Team mehrere  
<a href="https://debconf21.debconf.org/schedule/mobile/">Mobilgerät-freundliche Optionen zum Verfolgen des Zeitplans</a>
zur Verfügung.
</p>

<p>
Für diejenigen, die nicht teilnehmen konnten, sind die meisten Vorträge und
Veranstaltungen bereits auf der 
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2021/DebConf21/">Debian-Meetings-Archivwebsite</a> zu finden.
Die restlichen erscheinen dort in den kommenden Tagen.
</p>

<p>
Die <a href="https://debconf21.debconf.org/">DebConf21</a>-Website bleibt
zu Archivzwecken aktiv und wird auch weiterhin Links zu den
Präsentationen und Videos der Vorträge und Veranstaltungen vorhalten.
</p>

<p>
Für die <a href="https://wiki.debian.org/DebConf/22">DebConf22</a> im nächsten 
Jahr ist geplant, sie im Juli 2022 in Prizren im Kosovo stattfinden zu lassen.
</p>

<p>
Die DebConf hat sich dem Ziel verschrieben, eine sichere und angenehme Umgebung
für alle, die dabei sind, zu schaffen. Während der Konferenz waren mehrere Teams
(Front Desk, Willkommens- und Gemeinschafts-Team) beratend zur Stelle, um allen
Anwesenden einen angenehmen Aufenthalt zu ermöglichen und Lösungen für allerlei
auftretende Probleme zu finden. Genauere Details dazu finden Sie auf der
<a href="https://debconf21.debconf.org/about/coc/">Website mit dem DebConf21-Verhaltenskodex.</a>
</p>

<p>
Debian dankt den vielen
<a href="https://debconf21.debconf.org/sponsors/">Sponsoren</a>, die geholfen
haben, die DebConf21 auf die Beine zu stellen, insbesondere unseren Platin-Sponsoren:

<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://code4life.roche.com/">Roche</a>,
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>
und <a href="https://google.com/">Google</a>
</p>

<h2>Über Debian</h2>

<p>Das Debian-Projekt wurde 1993 von Ian Murdock als wirklich freies 
Gemeinschaftsprojekt gegründet. Seitdem ist das Projekt zu einem der größten 
und einflussreichsten Open-Source-Projekte angewachsen. Tausende von 
Freiwilligen aus aller Welt arbeiten zusammen, um Debian-Software herzustellen 
und zu betreuen. Verfügbar in über 70 Sprachen und eine große Bandbreite an 
Rechnertypen unterstützend bezeichnet sich Debian als das <q>universelle 
Betriebssystem</q>.</p>


<h2>Über die DebConf</h2>

<p>
Die DebConf ist die Entwicklerkonferenz des Debian-Projekts. Neben einem 
mit technischen, sozialen und politischen Vorträgen gefüllten Zeitplan bietet 
die DebConf eine Möglichkeit für Entwicklerinnen, Unterstützer und andere 
Interessierte, sich persönlich zu treffen und enger zusammenzuarbeiten. Sie 
findet seit 2000 jedes Jahr an verschiedenen Orten wie Schottland, Argentinien 
und Bosnien-Herzegowina statt. Weitere Informationen zur DebConf sind unter 
<a href="https://debconf.org/">https://debconf.org</a> verfügbar.
</p>


<h2>Über Lenovo</h2>

<p>
Als globaler Technikanführer, der ein breites Portfolio von 
vernetzten Produkten wie Smartphones, Tablets, PCs und Workstations sowie  
AR-/VR-Geräte, Smart Home/Office und Rechenzentrumslösungen herstellt, ist sich  
<a href="https://www.lenovo.com">Lenovo</a> bewusst,  
wie kritisch offene Systeme und Plattformen für eine vernetzte Welt sind.
</p>


<h2>Über Infomaniak</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> ist das größte 
Webhosting-Unternehmen der Schweiz. Es bietet unter anderem Backup- und 
Speicherdienste sowie Lösungen für Veranstaltungen, Live-Streaming und 
Video-on-Demand-Dienste an. Alle seine Rechenzentren und sämtliche 
Bestandteile, deren Funktionieren für die Dienste und Produkte (sowohl Hard- 
als auch Software) unerlässlich sind, sind vollständig im Besitz der Informaniak.
</p>


<h2>Über Roche</h2>
<p><a href="https://code4life.roche.com/">Roche</a> ist ein großer Anbieter
für Pharmazeutik und ein Forschungsunternehmen mit Fokus auf personalisierte
Gesundheitsfürsorge. Mehr als 100.000 Angestellte in aller Welt suchen unter
Einsatz von Wissenschaft und Technik Lösungen für einige der größten
Herausforderungen der Menschheit. Roche ist stark in öffentlich finanzierten
gemeinsamen Forschungsprojekten mit anderen industriellen und akademischen
Partnern eingebunden und unterstützt die DebConf seit 2017.
</p>

<h2>Über Amazon Web Services (AWS)</h2>

<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> ist eine der 
umfangreichsten und am häufigsten eingesetzten Cloud-Plattformen. Sein Angebot 
umfasst mehr als 175 Komplettdienste von global verteilten Rechenzentren 
(in 77 Verfügbarkeitszonen, die sich in 24 geografischen Regionen befinden). 
Zu den AWS-Kunden gehören sowohl die am schnellsten wachsenden Startups als 
auch die größten Unternehmen und führenden Regierungsorganisationen.
</p>


<h2>Über Google</h2>

<p>
<a href="https://google.com/">Google</a> ist eines der größten Technik- 
Unternehmen der Welt; sein Angebot umfasst sehr viele verschiedene 
Internet-bezogene Dienste und Produkte wie Onlinewerbung, Suche, Cloud- 
Computing, Software und Hardware.
</p>

<p>
Google unterstützt Debian schon seit mehr als zehn Jahren als 
DebConf-Sponsor und betreibt darüber hinaus als Debian-Partner Teile der 
 <a href="https://salsa.debian.org">Salsa</a>-Continuous-Integration-Infrastruktur auf der Google-Cloud-Plattform.
</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen sie bitte die DebConf21-Website unter 
<a href="https://debconf20.debconf.org/">https://debconf21.debconf.org/</a>
oder senden eine E-Mail (auf Englisch) an &lt;press@debian.org&gt;.</p>
