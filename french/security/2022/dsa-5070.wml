#use wml::debian::translation-check translation="08e35e9feeb931b15908371dcf74a977bc01c41d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4122">CVE-2021-4122</a>

<p>Milan Broz, son responsable, a découvert un problème dans cryptsetup,
l'outil de configuration de chiffrement de disque pour Linux.</p>

<p>Le renouvellement de chiffrement en ligne LUKS2 (un format sur disque)
est une extension optionnelle pour permettre à un utilisateur de modifier
la clé de renouvellement de chiffrement des données tandis que le
périphérique de données est disponible à l'usage pendant tout le processus
de renouvellement.</p>

<p>Un attaquant peut modifier des métadonnées sur le disque pour simuler un
déchiffrement en cours avec une étape de renouvellement de chiffrement
plantée (inachevée) et déchiffrer continuellement une partie du
périphérique LUKS2 (les périphériques LUKS1 sont aussi indirectement
affectés, voir plus bas).</p>

<p>Cette attaque nécessite un accès physique répété au périphérique LUKS2
mais aucune connaissance de la phrase secrète.</p>

<p>L'étape de déchiffrement est réalisée après qu'un utilisateur valide a
activé le périphérique avec une phase secrète correcte et des métadonnées
modifiées.</p>

<p>La taille des données déchiffrées par étape d'attaque dépend de la
taille de l'en-tête LUKS2 configurée (la taille des métadonnées est
configurable pour LUKS2). Avec les paramètres par défaut de LUKS2 (en-tête
de 16 Mio) et un seul « keyslot » alloué (clé 512 bits pour AES-XTS), un
déchiffrement simulé avec une résilience de somme de contrôle SHA1 (somme
de contrôle de 20 octets pour des blocs de 4096 octets), la taille maximale
déchiffrée peut être supérieure à 3 Gio.</p>

<p>L'attaque ne s'applique pas au format LUKS1, mais l'attaquant peut
mettre à jour les métadonnées en place vers le format LUKS2 comme étape
supplémentaire. Pour un en-tête LUKS2 converti de cette façon, la zone de
« keyslot » est limitée à la taille déchiffrée (avec une somme de contrôle
SHA1) au-dessus de 300 Mio.</p>

<p>Les périphériques LUKS, formatés avec un binaire de cryptsetup venant de
Debian Stretch ou d'une version précédente, utilisent LUKS1. Néanmoins,
depuis Debian Buster le format de LUKS sur disque est LUKS2. En
particulier, les périphériques chiffrés formatés par l'installateur de
Debian Buster et Bullseye utilisent LUKS2 par défaut.</p></li>

<li>Troncature de clé dans dm-integrity

<p>Cette mise à jour corrige en plus un problème de troncature de clé pour
les périphériques dm-integrity autonomes utilisant la protection
d'intégrité HMAC. Pour ce type de périphérique avec des clés HMAC très
longues (généralement &gt;106 octets de longueur), il peut être nécessaire
de tronquer manuellement la clé en utilisant l'option
<q>--integrity-key-size</q> d'integritysetup(8) afin de mapper correctement
le périphérique avec les versions 2:2.3.7-1+deb11u1 et suivantes.</p>

<p>Seuls les périphériques « dm-integrity » autonomes sont affectés. Les
périphériques « dm-crypt », y compris ceux utilisant un chiffrement de
disque authentifié, ne sont pas affectés.</p></li>

</ul>

<p>Pour la distribution oldstable distribution (Buster), ce problème n'est
pas présent.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 2:2.3.7-1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cryptsetup.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cryptsetup, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cryptsetup">\
https://security-tracker.debian.org/tracker/cryptsetup</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5070.data"
# $Id: $
