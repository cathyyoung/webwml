#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4008">CVE-2016-4008</a>

<p>Une boucle infinie a été découverte lors de l'analyse de certificats
DER. La fonction _asn1_extract_der_octet dans lib/decoding.c dans Libtasn1
de GNU antérieure à 4.8, lorsqu'elle est utilisée sans l'indicateur
ASN1_DECODE_FLAG_STRICT_DER, permet à des attaquants distants de provoquer
un déni de service (récursion infinie) à l'aide d'un certificat contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.13-2+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libtasn1-3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-495.data"
# $Id: $
